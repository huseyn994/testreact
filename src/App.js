import React from 'react';
import CustomRouter from './components/CustomRouter';

const App = () => (
    <div>
      <CustomRouter />
    </div>
)
export default App;
