import React from 'react';
import { Link } from 'react-router-dom'
import UserApi from '../api'
import Users from './Users'
import NotFound from './404';
const renderModal=()=>{
  
}
const User_in = (props) => {
  const user = UserApi.get(
    parseInt(props.match.params.id, 10)
  )
  if (!user) {
    return <NotFound/>
  }
  return (

    <div className="user_in">
        <div className="container">
          <div className="user_in_image"><img src={user.avatar} /></div>
        </div>
        <h1>{user.name}</h1>
        <div className="user_in_desc">{user.description}</div>
        <Link to={`/`}>Home page</Link>
    </div> 
    
  )
}

export default User_in;