import React from 'react';
import UserApi from '../api'; // Fake API of users.
import User from './User'; // User component.

const Users = () => {
    return (
        <div className="row">
            {/*Fetching users from API*/}
            {UserApi.all().map((item, index) => (
                <User key={index} index={index} options={item} />
            ))}
        </div>
    );
}
export default Users;