import React from 'react';
import { Switch, Route } from 'react-router-dom'
import Users from './Users'; // Users page
import User_in from './User_in'; // User In page
import NotFound from './404'; // 404 Not found
const CustomRouter = () => {
    return (
      <div className="wrapper">
        <div className="container">
            {/*Content chance occurs in this Switch*/}
            <Switch>
              <Route exact path='/' component={Users}/>
              <Route path='/user/:id' component={User_in}/>
              <Route path="*" component={NotFound}/>{/* Otherwise this route will be triggered */}
            </Switch>
        </div>
      </div>
    )
}; 
export default CustomRouter;