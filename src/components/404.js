import React from 'react';
import { Link } from 'react-router-dom'
const NotFound = () => {
    return (
        <div className="no_user">
             <p>Sorry, but the page was not found</p>
            <Link to={`/`}>Home page</Link>
        </div>
    );
}
export default NotFound;