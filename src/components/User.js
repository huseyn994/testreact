import React from 'react';
import { Link } from 'react-router-dom'
import UserApi from '../api' // Fake API of users.

const User = (props)=>{
    return (
        <div className="col-md-3 col-lg-3 col-xl-3">
            <div className="user">
                {/*Generating user*/}   
                <div className="user_image">
                    <Link to={`/user/${props.options.id}`}>
                        <img src={props.options.avatar} />
                    </Link>                
                </div>         
                <div className="user_name">
                    <h3><Link to={`/user/${props.options.id}`}>{props.options.name}</Link></h3>
                </div>
            </div>
        </div>         
    );
}
export default User